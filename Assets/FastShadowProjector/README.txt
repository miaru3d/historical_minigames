======================================================
Fast Shadow Projector package for Unity by Grimworks
======================================================


Open Documentation.pdf included in root directory of this package for a basic guide using Fast Shadow Projector.



For best documentation, latest info, etc. please visit:

http://thegrimworks.com/FastShadowProjector/site/



Online tutorial version:

http://thegrimworks.com/FastShadowProjector/site/?page_id=5



Supporting tutorial video:

https://www.youtube.com/watch?v=qxj936vDFZY



Any questions? please drop us a line: support@thegrimworks.com




This is a much faster, easy-to-use alternative to the <strong>Blob Shadow Projector. </strong> If you need to render lots of <strong>blob shadows</strong>  that <strong>fall correctly</strong> on <strong>any surface</strong>   while still maintaining high framerate even on mobile devices – look no further.<br><br>> Shadows fall correctly on any surface<br>> Many shadows on a mesh with one draw call <br>> Highly customizable: opacity, shape, size, etc. <br>> Shadows maintain object’s orientation <br>> No quads hovering above surfaces <br>> Great performance!<br><br>
