﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	
	void OnGUI() {
		if (GUI.Button(new Rect(10, 10, 100, 20), "General")) {
			Application.LoadLevel("GeneralScene");
		}

		if (GUI.Button(new Rect(130, 10, 100, 20), "Many shadows")) {
			Application.LoadLevel("ManyShadowsScene");
		}
	}
}