﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ShadowProjector))] 
public class ShadowProjectorEditor : Editor {

	string[] _ShadowResOptions = new string[] { "Very Low", "Low", "Medium", "High", "Very High" };

	string[] _CullingOptions = new string[] { "None", "Caster bounds", "Projection volume" };

	Rect UVRect;
	
	public override void OnInspectorGUI() {
		serializedObject.Update ();

		ShadowProjector shadowProj = (ShadowProjector) target;

		shadowProj.GlobalProjectionDir = EditorGUILayout.Vector3Field("Global light direction", shadowProj.GlobalProjectionDir, null);
	
		shadowProj.GlobalShadowResolution = EditorGUILayout.Popup("Global shadow resolution", shadowProj.GlobalShadowResolution, _ShadowResOptions); 
	
		shadowProj.GlobalShadowCullingMode = (GlobalProjectorManager.ProjectionCulling)EditorGUILayout.Popup("Global culling mode", (int)shadowProj.GlobalShadowCullingMode, _CullingOptions); 

		shadowProj.ShadowSize = EditorGUILayout.FloatField("Shadow size", shadowProj.ShadowSize);
		
		shadowProj.ShadowColor = EditorGUILayout.ColorField("Shadow color", shadowProj.ShadowColor);
		
		shadowProj.ShadowOpacity = EditorGUILayout.Slider("Shadow opacity", shadowProj.ShadowOpacity, 0.0f, 1.0f);

		shadowProj._Material = (Material)EditorGUILayout.ObjectField("Shadow material", (Object)shadowProj._Material, typeof(Material), false, null);

		shadowProj.ShadowLocalOffset = EditorGUILayout.Vector3Field("Shadow local offset", shadowProj.ShadowLocalOffset, null);
		shadowProj.RotationAngleOffset = EditorGUILayout.FloatField("Rotation angle offset", shadowProj.RotationAngleOffset);
		EditorGUILayout.LabelField("Shadow UV Rect");

		UVRect = shadowProj.UVRect;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("X:", GUILayout.MaxWidth(15));
		UVRect.x = EditorGUILayout.FloatField(UVRect.x, GUILayout.ExpandWidth(true));
		EditorGUILayout.LabelField("Y:", GUILayout.MaxWidth(15));
		UVRect.y = EditorGUILayout.FloatField(UVRect.y, GUILayout.ExpandWidth(true));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("W:", GUILayout.MaxWidth(15));
		UVRect.width = EditorGUILayout.FloatField(UVRect.width , GUILayout.ExpandWidth(true));
		EditorGUILayout.LabelField("H:", GUILayout.MaxWidth(15));
		UVRect.height = EditorGUILayout.FloatField(UVRect.height, GUILayout.ExpandWidth(true));
		
		EditorGUILayout.EndHorizontal();

		shadowProj.UVRect = UVRect;

		if (!GlobalProjectorLayerExists()) {
			CheckGlobalProjectorLayer();
		}

		serializedObject.ApplyModifiedProperties();

		ApplyGlobalSettings();
	}

	public void OnEnable() {
		ShadowProjector targetShadowProj = (ShadowProjector)target;
		Object[] shadowProjectors = GameObject.FindObjectsOfType(typeof(ShadowProjector));
		
		foreach (ShadowProjector shadowProj in shadowProjectors) {
			if (shadowProj.GetInstanceID() != targetShadowProj.GetInstanceID()) {
				targetShadowProj.GlobalProjectionDir = shadowProj.GlobalProjectionDir;
				targetShadowProj.GlobalShadowResolution = shadowProj.GlobalShadowResolution;
				targetShadowProj.GlobalShadowCullingMode = shadowProj.GlobalShadowCullingMode;
				
				break;
			}
		}
	}

	public void OnDisable() {
		ApplyGlobalSettings();
	}

	void ApplyGlobalSettings() {
		ShadowProjector targetShadowProj = (ShadowProjector)target;
		Object[] shadowProjectors = GameObject.FindObjectsOfType(typeof(ShadowProjector));
		
		foreach (ShadowProjector shadowProj in shadowProjectors) {
			if (shadowProj.GetInstanceID() != targetShadowProj.GetInstanceID()) {
				shadowProj.GlobalProjectionDir = targetShadowProj.GlobalProjectionDir;
				shadowProj.GlobalShadowResolution = targetShadowProj.GlobalShadowResolution;
				shadowProj.GlobalShadowCullingMode = targetShadowProj.GlobalShadowCullingMode;
			}
		}
	}

	void CheckGlobalProjectorLayer() {

		SerializedObject tagManager = new SerializedObject (AssetDatabase.LoadAllAssetsAtPath ("ProjectSettings/TagManager.asset")[0]);

		SerializedProperty it = tagManager.GetIterator ();
		while (it.Next(true)) {
			if (it.name.Contains("Layer") && it.name.Contains("User") && it.stringValue == "" ) {
	
				it.stringValue = GlobalProjectorManager.GlobalProjectorLayer;
				
				tagManager.ApplyModifiedProperties();
				break;
			}
		}
	}

	bool GlobalProjectorLayerExists() {
		SerializedObject tagManager = new SerializedObject (AssetDatabase.LoadAllAssetsAtPath ("ProjectSettings/TagManager.asset")[0]);
		
		SerializedProperty it = tagManager.GetIterator ();
		while (it.Next(true)) {
			if (it.name.Contains("Layer") && it.name.Contains("User") && it.stringValue.Contains(GlobalProjectorManager.GlobalProjectorLayer)) {
				return true;
			}
		}

		return false;
	}
}
