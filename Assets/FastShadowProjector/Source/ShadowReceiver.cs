﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Fast Shadow Projector/Shadow Receiver")]
public class ShadowReceiver : MonoBehaviour {
	
	MeshFilter _meshFilter;
	Mesh _mesh;
	Mesh _meshCopy;
	MeshRenderer _meshRenderer;
	
	void Awake() {
		_meshFilter = GetComponent<MeshFilter>();
		_meshRenderer = GetComponent<MeshRenderer>();
		_meshCopy = null;
	
		if (_meshFilter != null) {
			_mesh = _meshFilter.mesh;
		}
	}
	
	void Start () {	
		AddReceiver();
			
		if (_meshRenderer != null && _meshRenderer.isPartOfStaticBatch && _mesh != null) {
			CopyMesh();
		}
	}
	
	void CopyMesh() {
		_meshCopy = new Mesh();
		_meshCopy.vertices = _mesh.vertices;
		_meshCopy.normals = _mesh.normals;
		_meshCopy.uv = _mesh.uv;
		_meshCopy.triangles = _mesh.triangles;
		_meshCopy.tangents = _mesh.tangents;
		_meshCopy.colors = _mesh.colors;
		_meshCopy.colors32 = _mesh.colors32;
		_meshCopy.uv1 = _mesh.uv1;
		_meshCopy.uv2 = _mesh.uv2;
	}
	
	public Mesh GetMesh() {
		if (_meshCopy != null) {
			return _meshCopy;
		} else {
			return _mesh;
		}
	}
	
	void OnEnable() {
		AddReceiver();
	}
	
	void OnDisable() {
		RemoveReceiver();
	}
	
	void OnDestroy() {
		RemoveReceiver();
	}

	void AddReceiver() {
		if (_meshFilter != null) {
			GlobalProjectorManager.Get().AddReceiver(this);
		}
	}

	void RemoveReceiver() {
		if (GlobalProjectorManager.Exists()) {
			if (_meshFilter != null) {
				GlobalProjectorManager.Get().RemoveReceiver(this);
			}
		}
	}
}