﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalProjectorManager : MonoBehaviour {
	
	static string _GlobalProjectorShader = 
	"Shader \"GlobalProjector/Multiply\" {\n" + 
	"Properties {\n" +
		"_ShadowTex (\"Projector Texture\", 2D) = \"gray\" { TexGen ObjectLinear }\n" +
	"}\n" +
	"Subshader {\n"+
	  "Tags { \"RenderType\"=\"Transparent\" \"Queue\"=\"Geometry+1\" }\n" +
	  "Pass {\n" + 
		 "ZWrite Off\n" +
		 "ZTest LEqual\n" + 
		 "Fog { Color (1, 1, 1) }\n" +
		 "AlphaTest Greater 0\n" +
		 "ColorMask RGB\n" +
		 "Blend DstColor Zero\n" +
		 "Offset -1, -1\n" +
		 "SetTexture [_ShadowTex] {\n" +
			"combine texture, ONE - texture\n" +
			"Matrix [_GlobalProjector]\n" +
		  "}\n" +
		"}\n" +
	  "}\n" +
	"}\n";

	public enum ProjectionCulling {
		None,
		ProjectorBounds,
		ProjectionVolumeBounds
	}

	ProjectorEyeTexture _Tex;

	public Material _ProjectorMaterial;
	
	Matrix4x4 _ProjectorMatrix;
	Matrix4x4 _BiasMatrix;
	Matrix4x4 _ViewMatrix;
	Matrix4x4 _BPV;
	Matrix4x4 _ModelMatrix;
	Matrix4x4 _FinalMatrix;
	
	MaterialPropertyBlock _MBP;

	int[] _ShadowResolutions =  new int[] { 128, 256, 512, 1024, 2048 };

	public static readonly string GlobalProjectorLayer = "GlobalProjectorLayer";
	
	static GlobalProjectorManager _Instance;

	public static Vector3 GlobalProjectionDir {
		set {
			if (_Instance._GlobalProjectionDir != value) {
				_Instance._GlobalProjectionDir = value;
				_Instance.OnProjectionDirChange();
			}
		}

		get {
			return _Instance._GlobalProjectionDir;
		}
	}
	Vector3 _GlobalProjectionDir = new Vector3(0.0f, -1.0f, 0.0f);

	public static int GlobalShadowResolution {
		set {
			if (_Instance._GlobalShadowResolution != value) {
				_Instance._GlobalShadowResolution = value;
				_Instance.OnShadowResolutionChange();
			}
		}
		
		get {
			return _Instance._GlobalShadowResolution;
		}
	}
	int _GlobalShadowResolution = 2;

	public static ProjectionCulling GlobalShadowCullingMode {
		set {
			_Instance._GlobalShadowCullingMode = value;
		}
		
		get {
			return _Instance._GlobalShadowCullingMode;
		}
	}

	ProjectionCulling _GlobalShadowCullingMode = ProjectionCulling.None;
	
	Camera _ProjectorCamera;
	
	List<ShadowProjector> _ShadowProjectors;
	List<ShadowReceiver> _ShadowReceivers;
	
	public static GlobalProjectorManager Get() {
		if (_Instance == null) {
			_Instance = new GameObject("_GlobalProjectorManager").AddComponent<GlobalProjectorManager>();
			_Instance.Initialize();
		}
		return _Instance;
	}

	void Initialize() {
		gameObject.layer = LayerMask.NameToLayer(GlobalProjectorLayer);

		_ProjectorMaterial = new Material(_GlobalProjectorShader);

		_ProjectorCamera = gameObject.AddComponent<Camera>();
		_ProjectorCamera.clearFlags = CameraClearFlags.SolidColor;
		_ProjectorCamera.backgroundColor = new Color32(255, 255, 255, 0);
		_ProjectorCamera.cullingMask = 1 << LayerMask.NameToLayer(GlobalProjectorManager.GlobalProjectorLayer);
		_ProjectorCamera.orthographic = true;
		_ProjectorCamera.nearClipPlane = -10000;
		_ProjectorCamera.farClipPlane = 10000;
		_ProjectorCamera.aspect = 1.0f;
		_ProjectorCamera.depth = float.MinValue;

		CreateProjectorEyeTexture();

		_BiasMatrix = new Matrix4x4();
		_BiasMatrix.SetRow(0, new Vector4(0.5f, 0.0f, 0.0f, 0.5f));
		_BiasMatrix.SetRow(1, new Vector4(0.0f, 0.5f, 0.0f, 0.5f));
		_BiasMatrix.SetRow(2, new Vector4(0.0f, 0.0f, 0.5f, 0.5f));
		_BiasMatrix.SetRow(3, new Vector4(0.0f, 0.0f, 0.0f, 1.0f));

		_ProjectorMatrix = new Matrix4x4();

		_MBP = new MaterialPropertyBlock();

		_ShadowProjectors = new List<ShadowProjector>();
		_ShadowReceivers = new List<ShadowReceiver>();
	}

	void Awake() {
		OnProjectionDirChange();
	}

	void Start() {
		OnProjectionDirChange();       
	}

	void OnDestroy() {
		_Instance = null;
	}

	public static bool Exists() {
		return (_Instance != null);
	}

	public void AddProjector(ShadowProjector projector) {
		if (!_ShadowProjectors.Contains(projector)) {
			_ShadowProjectors.Add(projector);

			if (projector.GlobalProjectionDir != _GlobalProjectionDir) {
				GlobalProjectionDir = projector.GlobalProjectionDir;
			}
		}
	}

	public void RemoveProjector(ShadowProjector projector) {
		if (_ShadowProjectors.Contains(projector)) {
			_ShadowProjectors.Remove(projector);
		}
	}

	public void AddReceiver(ShadowReceiver receiver) {
		if (!_ShadowReceivers.Contains(receiver)) {
			_ShadowReceivers.Add(receiver);
		}
	}
	
	public void RemoveReceiver(ShadowReceiver receiver) {
		if (_ShadowReceivers.Contains(receiver)) {
			_ShadowReceivers.Remove(receiver);
		}
	}

	void OnProjectionDirChange() {
		if (camera != null) {
			camera.transform.rotation = Quaternion.LookRotation(_GlobalProjectionDir);
		}	
	}

	void OnShadowResolutionChange() {
		CreateProjectorEyeTexture();
	}

	void CreateProjectorEyeTexture() {
		_Tex = new ProjectorEyeTexture(_ProjectorCamera, _ShadowResolutions[_GlobalShadowResolution]);
		
		_ProjectorMaterial.SetTexture("_ShadowTex", _Tex.GetTexture());
	}

	void CalculateShadowBounds() {
		Plane[] mainCameraPlains = GeometryUtility.CalculateFrustumPlanes(Camera.main);

		Vector2 xRange = new Vector2(float.MaxValue, float.MinValue);
		Vector2 yRange = new Vector2(float.MaxValue, float.MinValue);

		Vector2 shadowCoords;
		float maxShadowSize = 0.0f;

		bool noVisibleProjectors = true;
		int projectorIndex = 0;
		int discardedCount = 0;

		Bounds projectorBounds = new Bounds();

		foreach (ShadowProjector shadowProjector in _ShadowProjectors) {
			switch(_GlobalShadowCullingMode) {
				case ProjectionCulling.ProjectorBounds: 
					if (!GeometryUtility.TestPlanesAABB(mainCameraPlains, shadowProjector.GetBounds())) {
						discardedCount++;
						shadowProjector.SetVisible(false);
						continue;
					}
					break;
				
				case ProjectionCulling.ProjectionVolumeBounds: 
					if (!IsProjectionVolumeVisible(mainCameraPlains, shadowProjector)) {
						discardedCount++;
					shadowProjector.SetVisible(false);
						continue;
					}
					break;

				default:
					break;
			}

			noVisibleProjectors = false;
			shadowProjector.SetVisible(true);

			shadowCoords = camera.WorldToViewportPoint(shadowProjector.GetShadowPos());

			if (projectorIndex == 0) {
				projectorBounds = new Bounds(shadowProjector.GetShadowPos(), Vector3.zero);
			} else {
				projectorBounds.Encapsulate(shadowProjector.GetShadowPos());
			}

			if (shadowCoords.x < xRange.x) xRange.x = shadowCoords.x;
			if (shadowCoords.x > xRange.y) xRange.y = shadowCoords.x;

			if (shadowCoords.y < yRange.x) yRange.x = shadowCoords.y;
			if (shadowCoords.y > yRange.y) yRange.y = shadowCoords.y;

			float shadowSize = shadowProjector.GetShadowWorldSize();

			if (shadowSize > maxShadowSize) {
				maxShadowSize = shadowSize;
			}

			projectorIndex++;
		}

		if (noVisibleProjectors) {
			return;
		}

		float cameraWorldSize = camera.orthographicSize * 2.0f;
		float maxShadowSizeViewport = maxShadowSize / cameraWorldSize;

		Vector3 camPos = projectorBounds.center;
		camera.transform.position = camPos;

		float maxRange = Mathf.Max(xRange[1] - xRange[0] + maxShadowSizeViewport * 2.0f, yRange[1] - yRange[0] + maxShadowSizeViewport* 2.0f);
		camera.orthographicSize = camera.orthographicSize * maxRange;
	}

	bool IsProjectionVolumeVisible(Plane[] planes, ShadowProjector projector) {
		float boundSize = 1000000.0f;

		Vector3 center = projector.GetShadowPos() + GlobalProjectionDir.normalized * (boundSize * 0.5f);
		Vector3 size = new Vector3(Mathf.Abs(GlobalProjectionDir.normalized.x), Mathf.Abs(GlobalProjectionDir.normalized.y), Mathf.Abs(GlobalProjectionDir.normalized.z))  * boundSize;
		Bounds bounds = new Bounds(center, size);

		float shadowSize = projector.GetShadowWorldSize();

		bounds.Encapsulate(new Bounds(projector.GetShadowPos(), new Vector3(shadowSize, shadowSize, shadowSize)));

		return GeometryUtility.TestPlanesAABB(planes, bounds);
	}

	void LateUpdate() {
		if (_ShadowProjectors.Count > 0 && _ShadowReceivers.Count > 0) {
			CalculateShadowBounds();
			
			float n = camera.nearClipPlane;
			float f = camera.farClipPlane;
			float r = camera.orthographicSize;
			float t = camera.orthographicSize;
			
			_ProjectorMatrix.SetRow(0, new Vector4(1 / r, 0.0f, 0.0f, 0));
			_ProjectorMatrix.SetRow(1, new Vector4(0.0f, 1 / t, 0.0f, 0));
			_ProjectorMatrix.SetRow(2, new Vector4(0.0f, 0.0f, -2 / (f - n), 0));
			_ProjectorMatrix.SetRow(3, new Vector4(0.0f, 0.0f, 0.0f, 1.0f));
			
			_ViewMatrix = camera.transform.localToWorldMatrix.inverse;
			
			_BPV =  _BiasMatrix * _ProjectorMatrix * _ViewMatrix;
			
			RenderShadows();
		}
	}
		
	void RenderShadows() {
		bool useMBP = true; // WP8 doesn't support MBP's correctly - only one receiver will work for now.

#if UNITY_WP8
		useMBP = false;
#endif
		
		for (int i = 0; i < _ShadowReceivers.Count; i++) {
			_ModelMatrix = _ShadowReceivers[i].transform.localToWorldMatrix;
			_FinalMatrix = _BPV * _ModelMatrix;
			
			if (useMBP) {
	
				_MBP.AddMatrix("_GlobalProjector", _FinalMatrix);
				Graphics.DrawMesh( _ShadowReceivers[i].GetMesh(), _ModelMatrix, _ProjectorMaterial, LayerMask.NameToLayer("Default"), null, 0, _MBP);
			} else {
				_ProjectorMaterial.SetMatrix("_GlobalProjector", _FinalMatrix);
				Graphics.DrawMesh(_ShadowReceivers[i].GetMesh(), _ModelMatrix, _ProjectorMaterial, LayerMask.NameToLayer("Default"));
			}
		}
	}

	void OnPreCull() {
		foreach (ShadowProjector shadowProjector in _ShadowProjectors) {
			shadowProjector.SetVisible(true);
			shadowProjector.OnPreRender();
		}
	}

	void OnPostRender() {
		_Tex.GrabScreenIfNeeded();

		foreach (ShadowProjector shadowProjector in _ShadowProjectors) {
			shadowProjector.SetVisible(false);
		}
	}
}