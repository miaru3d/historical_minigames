﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Fast Shadow Projector/Shadow Projector")]
public class ShadowProjector : MonoBehaviour {

	private static class MeshGen {
		
		public static Mesh CreatePlane(Vector3 up, Vector3 right, Rect uvRect, Color color) {
			Mesh planeMesh = new Mesh();
			
			Vector3[] vertices = new Vector3[] {
				(up * 0.5f - right * 0.5f),
				(up * 0.5f + right * 0.5f),
				(-up * 0.5f - right * 0.5f),
				(-up * 0.5f + right * 0.5f)
			};
			
			Vector2[] uvs = new Vector2[] {
				new Vector2(uvRect.x, uvRect.y + uvRect.height),
				new Vector2(uvRect.x + uvRect.width, uvRect.y + uvRect.height),
				new Vector2(uvRect.x, uvRect.y),
				new Vector2(uvRect.x + uvRect.width, uvRect.y),
			};

			Color[] colors = new Color[] {
				color,
				color,
				color,
				color
			};

			int[] indices = new int[] { 0, 1, 3, 0, 3, 2 };
			
			planeMesh.vertices = vertices;
			planeMesh.uv = uvs;
			planeMesh.colors = colors;
			planeMesh.SetTriangles(indices, 0);
			
			return planeMesh;
		}
	}
	
	public Vector3 GlobalProjectionDir {
		set {
			_GlobalProjectionDir = value;

			if (GlobalProjectorManager.Exists()) {
				GlobalProjectorManager.GlobalProjectionDir = _GlobalProjectionDir; 
			}
		}
		get {
			return _GlobalProjectionDir;
		}
	}

	[UnityEngine.SerializeField]
	protected Vector3 _GlobalProjectionDir = new Vector3(0.0f, -1.0f, 0.0f);

	public int GlobalShadowResolution {
		set {
			_GlobalShadowResolution = value;
			
			if (GlobalProjectorManager.Exists()) {
				GlobalProjectorManager.GlobalShadowResolution = _GlobalShadowResolution; 
			}
		}
		get {
			return _GlobalShadowResolution;
		}
	}
	
	[UnityEngine.SerializeField]
	protected int _GlobalShadowResolution = 1;

	public GlobalProjectorManager.ProjectionCulling GlobalShadowCullingMode {
		set {
			_GlobalShadowCullingMode = value;
			
			if (GlobalProjectorManager.Exists()) {
				GlobalProjectorManager.GlobalShadowCullingMode = _GlobalShadowCullingMode; 
			}
		}
		get {
			return _GlobalShadowCullingMode;
		}
	}
	
	[UnityEngine.SerializeField]
	protected GlobalProjectorManager.ProjectionCulling _GlobalShadowCullingMode;
	
	public float ShadowSize {
		set {
			if (_ShadowSize != value) {
				_ShadowSize = value;
				if (_ShadowDummyMesh != null) {
					OnShadowSizeChanged();
				}
			}
		}

		get {
			return _ShadowSize;
		}
	}

	[UnityEngine.SerializeField]
	float _ShadowSize = 1.0f;
	
	public Color ShadowColor {
		set {
			if (_ShadowColor != value) {
				_ShadowColor = value;
				if (_ShadowDummyMesh != null) {
					OnShadowColorChanged();
				}
			}
		}
		
		get {
			return _ShadowColor;
		}
	}
	
	[UnityEngine.SerializeField]
	Color _ShadowColor = new Color(1.0f, 1.0f, 1.0f);

	public float ShadowOpacity {
		set {
			if (_ShadowOpacity != value) {
				_ShadowOpacity = value;
				if (_ShadowDummyMesh != null) {
					OnShadowColorChanged();
				}
			}
		}
		
		get {
			return _ShadowOpacity;
		}
	}
	
	[UnityEngine.SerializeField]
	float _ShadowOpacity = 1.0f;
	
	public Material _Material; 

	public Vector3 ShadowLocalOffset {
		set {
			_ShadowLocalOffset = value;
			
			if (_ShadowDummy != null) {
				_ShadowDummy._ShadowLocalOffset = _ShadowLocalOffset;
			}
		}
		
		get {
			return _ShadowLocalOffset;
		}
	}

	[UnityEngine.SerializeField]
	Vector3 _ShadowLocalOffset;

	public float RotationAngleOffset {
		set {
			_RotationAngleOffset = value;

			if (_ShadowDummy != null) {
				_ShadowDummy._RotationAngleOffset = _RotationAngleOffset;
			}
		}

		get {
			return _RotationAngleOffset;
		}
	}

	[UnityEngine.SerializeField]
	float _RotationAngleOffset;
	

	public Rect UVRect {
		set {
			_UVRect = value;
			if (_ShadowDummy != null) {
				OnUVRectChanged();
			}
		}
		
		get {
			return _UVRect;
		}
	}

	[UnityEngine.SerializeField]
	Rect _UVRect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);

	MeshRenderer _Renderer;
	MeshFilter _MeshFilter;
	Mesh _ShadowDummyMesh;

	ProjectorShadowDummy _ShadowDummy;

	void Awake() {
		_ShadowDummyMesh = MeshGen.CreatePlane(new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f), _UVRect, new Color(_ShadowColor.r, _ShadowColor.g, _ShadowColor.b, _ShadowOpacity));

		Transform parent = transform;

		_ShadowDummy = new GameObject("shadowDummy").AddComponent<ProjectorShadowDummy>();
		_ShadowDummy.transform.parent = parent;
		_ShadowDummy.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

		_ShadowDummy.gameObject.layer = LayerMask.NameToLayer(GlobalProjectorManager.GlobalProjectorLayer);

		_ShadowDummy._ShadowLocalOffset = _ShadowLocalOffset;
		_ShadowDummy._RotationAngleOffset = _RotationAngleOffset;

		OnShadowSizeChanged();

		_Renderer = _ShadowDummy.gameObject.AddComponent<MeshRenderer>();
		_Renderer.receiveShadows = false;
		_Renderer.castShadows = false;
		_Renderer.material = _Material;

		_MeshFilter = _ShadowDummy.gameObject.AddComponent<MeshFilter>();
		_MeshFilter.mesh = _ShadowDummyMesh;
	}

	void Start () {	
		GlobalProjectorManager.Get().AddProjector(this);
	}

	void OnEnable() {
		GlobalProjectorManager.Get().AddProjector(this);
	}

	void OnDisable() {
		if (GlobalProjectorManager.Exists()) {
			GlobalProjectorManager.Get().RemoveProjector(this);
		}
	}

	void OnDestroy() {
		if (GlobalProjectorManager.Exists()) {
			GlobalProjectorManager.Get().RemoveProjector(this);
		}
	}

	public Bounds GetBounds() {
		return _Renderer.bounds;
	}

	public bool IsVisible() {
		return _Renderer.isVisible;
	}

	public void SetVisible(bool visible) {
		_Renderer.enabled = visible;
	}

	public void OnPreRender() {
		if (_ShadowDummy != null) {
			_ShadowDummy.OnPreRender();
		}
	}

	public Matrix4x4 ShadowDummyLocalToWorldMatrix() {
		return _ShadowDummy.transform.localToWorldMatrix;
	}

	public float GetShadowWorldSize() {
		return ShadowSize * (ShadowDummyLocalToWorldMatrix() * new Vector3(1.0f, 0.0f, 0.0f)).magnitude;
	}

	public Vector3 GetShadowPos() {
		return _ShadowDummy.transform.position;
	}

	void OnShadowSizeChanged() {
		_ShadowDummy.transform.localScale = new Vector3(_ShadowSize, _ShadowSize, _ShadowSize);
	}

	void OnUVRectChanged() {
		RebuildMesh();
	}

	public void OnShadowColorChanged() {
		Color color = new Color(_ShadowColor.r, _ShadowColor.g, _ShadowColor.b, _ShadowOpacity);
		_ShadowDummyMesh.colors = new Color[] { color, color, color, color };
	}

	void RebuildMesh() {
		_ShadowDummyMesh = MeshGen.CreatePlane(new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f), _UVRect, new Color(_ShadowColor.r, _ShadowColor.g, _ShadowColor.b, _ShadowOpacity));
		_MeshFilter.mesh = _ShadowDummyMesh;
	}
}

