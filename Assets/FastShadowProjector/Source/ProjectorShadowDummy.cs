﻿using UnityEngine;
using System.Collections;

public class ProjectorShadowDummy : MonoBehaviour {

	public Vector3 _ShadowLocalOffset;
	public float _RotationAngleOffset;

	Quaternion _AngleOffset;

	Quaternion _InbetweenRotation;

	bool _RotationOffsetApplied = false;

	public void OnPreRender() {
		if (_RotationAngleOffset != 0.0f) {

			_AngleOffset = Quaternion.AngleAxis(_RotationAngleOffset, GlobalProjectorManager.Get().camera.transform.forward);
			
			_InbetweenRotation = Quaternion.LookRotation(GlobalProjectorManager.Get().camera.transform.forward, (transform.rotation) * new Vector3(0.0f, 1.0f, 0.0f));
			transform.rotation =  _AngleOffset * _InbetweenRotation; 
			
			_RotationOffsetApplied = true;
		} else {
			transform.rotation = Quaternion.LookRotation(GlobalProjectorManager.Get().camera.transform.forward, transform.rotation * new Vector3(0.0f, 1.0f, 0.0f));
		}

		transform.localPosition = _ShadowLocalOffset;
	}

	void OnRenderObject() {
		if (_RotationOffsetApplied) {
			transform.rotation = _InbetweenRotation;
			_RotationOffsetApplied = false;
		}
	}
}

